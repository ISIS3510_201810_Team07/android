package com.leptons.keepmesafe;

import android.app.Application;

import com.leptons.keepmesafe.di.component.AppComponent;
import com.leptons.keepmesafe.di.component.DaggerAppComponent;
import com.leptons.keepmesafe.di.module.AppModule;

public class App extends Application {

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        AppModule appModule = new AppModule(this);
        mAppComponent = DaggerAppComponent.builder().appModule(appModule).build();
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
