package com.leptons.keepmesafe.profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.leptons.keepmesafe.R;


public class ProfileActivity extends AppCompatActivity  {

    private String id;
    private EditText nombre,correo, numero, metros,tiempo;
    private DatabaseReference mDatabase,userContact;
    private FirebaseAuth mAuth;


    SharedPreferences myPreferences;
    SharedPreferences.Editor myEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        myPreferences= getSharedPreferences("MisPreferencias",Context.MODE_PRIVATE);
        myEditor = myPreferences.edit();

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        nombre = findViewById(R.id.nombre);
        correo = findViewById(R.id.contacto);
        numero = findViewById(R.id.telefono);
        metros = findViewById(R.id.metros);
        tiempo = findViewById(R.id.tiempo);

        mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if(dataSnapshot.child("correo").getValue()!= null )
                {
                    correo.setText(myPreferences.getString("Email",""));
                }
                if(dataSnapshot.child("phone").getValue()!= null)
                {
                    numero.setText(myPreferences.getString("Telefono",""));
                }
                if(dataSnapshot.child("name").getValue()!= null)
                {
                    nombre.setText(myPreferences.getString("Nombre",""));
                }
            }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
                });




    }


    public void agregarContacto (View view)
    {
        mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if(snapshot.child("phone").getValue().toString()!= numero.getText().toString() )
                {
                    mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).child("phone").setValue(numero.getText().toString());

                }

                if(snapshot.child("name").getValue().toString()!= nombre.getText().toString() )
                {
                    mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).child("name").setValue(nombre.getText().toString());
                }

                if(snapshot.child("correo").getValue().toString()!= correo.getText().toString())
                {
                    mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).child("correo").setValue(correo.getText().toString());
                }


            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        myEditor.putString("Nombre",nombre.getText().toString());
        myEditor.putString("Telefono",numero.getText().toString());
        myEditor.putString("Email",correo.getText().toString());
        myEditor.commit();
        View vista = this.getCurrentFocus();
        if (vista != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(vista.getWindowToken(), 0);
        }
        Toast.makeText(this, "Contacto agregado", Toast.LENGTH_SHORT).show();

    }
    public void agregarExcepciones(View view){

        View vista = this.getCurrentFocus();
        if (vista != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(vista.getWindowToken(), 0);
        }
        Toast.makeText(this, "Excepción agregada", Toast.LENGTH_SHORT).show();


    }

}
