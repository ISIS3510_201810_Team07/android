package com.leptons.keepmesafe.sharelocation;

import android.annotation.SuppressLint;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.ConnectionResult;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.content.pm.PackageManager;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.leptons.keepmesafe.App;
import com.leptons.keepmesafe.R;
import com.leptons.keepmesafe.common.PlaceAutocompleteAdapter;
import com.leptons.keepmesafe.common.ShakeDetector;
import com.leptons.keepmesafe.common.Util;
import com.leptons.keepmesafe.ViewModelFactory;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.leptons.keepmesafe.data.rxfirestore.RxFirestore;
import com.leptons.keepmesafe.model.SharedLocation;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;

import io.reactivex.Observable;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;



public class ShareLocationActivity extends AppCompatActivity
        implements OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener {

    private static final float DEFAULT_ZOOM_LEVEL = 12f;
    private static final int PERMISSION_SEND_SMS = 123;
    private static final String TAG = "ShareLocationActivity";
    private String contactPhone, mensaje, userEmail, userUID;
    private static ConnectivityManager manager;

    private FirebaseUser mUser;
    private AutoCompleteTextView searchApi;
    private DocumentReference mUserDocRef;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;
    private Marker mMyLocMarker;
    private GoogleMap mGoogleMap;
    private GoogleApiClient mGoogleApiClient;
    private boolean mFollowMarker;
    private Disposable mLocationUpdateDisposable;
    private DatabaseReference mDatabase,userContact;
    private FirebaseAuth mAuth;
    private PlaceAutocompleteAdapter placeAutocompleteAdapter;
    private static final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(
            new LatLng(-40, -168), new LatLng(71, 136));

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.text_dev_id)
    TextView mTextDevId;
    @BindView(R.id.text_curr_latlng)
    TextView mTextLatLng;
    @BindView(R.id.btn_broadcast)
    Button mBtnBroadcast;

    @Inject
    ViewModelFactory mViewModelFactory;
    ShareLocationViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_location);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        userEmail = mAuth.getCurrentUser().getEmail();
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        mUserDocRef = FirebaseFirestore.getInstance()
                    .collection("users")
                    .document(mUser.getUid());

        mUserDocRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null) {
                    userUID = document.getString("devId");
                    } else {
                        Log.d("LOGGER", "No such document");
                    }
                }

            }
        });

        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.share_location);
        }

        ((App) getApplication()).getAppComponent().inject(this);
        initViewModel();

        mSensorManager = (SensorManager) getSystemService(this.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake(int count) {
                handleShakeEvent(count);
            }
        });

        mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if( dataSnapshot.child("phone").getValue()!= null )
                {
                    contactPhone =  dataSnapshot.child("phone").getValue().toString();
                }
                else
                {
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

    }
    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    private void requestSmsPermission(String message)
    {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{ android.Manifest.permission.SEND_SMS},
                    PERMISSION_SEND_SMS);
        } else {
            if(contactPhone != null || contactPhone != "")
            {
                sendSms(contactPhone, message);
            }
        }
    }

    private void init()
    {
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        placeAutocompleteAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, LAT_LNG_BOUNDS, null);
        searchApi.setAdapter(placeAutocompleteAdapter);
    }


    private void sendSms(String phoneNumber, String message){
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, null, null);
    }

    public void handleShakeEvent(int counter)
    {
        mensaje = "El usuario" + userEmail+  "ha activado el PROCEDIMIENTO  DE PANICO su codigo de tracking es:";
        requestSmsPermission(mensaje);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Util.checkGooglePlayServicesAvailability(this) &&
                Util.checkLocationPermission(this)) {
            subscribeToLocationUpdate();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mLocationUpdateDisposable.dispose();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mViewModel.stopSharingLocation();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (Util.checkLocationPermissionsResult(requestCode, permissions, grantResults)) {
            subscribeToLocationUpdate();
        }

        else if(requestCode == PERMISSION_SEND_SMS && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED )
        {
            sendSms(contactPhone, mensaje);
        }
            else {
            Toast.makeText(this, "Please grant permission to this app",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setOnMarkerClickListener(marker -> {
            mFollowMarker = true;
            return false;
        });
        mGoogleMap.setOnMapClickListener(latLng -> mFollowMarker = false);
        if (mViewModel.getLastCachedLocation() != null)
            onLocationUpdated(mViewModel.getLastCachedLocation());
    }

    public void onBroadcastBtnClick(View view) {
        if(isOnline(this)) {
            if (mViewModel.isSharing()) {
                mViewModel.stopSharingLocation();
            } else {
                mViewModel.startSharingLocation();
                mensaje = "El usuario " + userEmail + " ha iniciado un recorrido si dessea hacer tracking use el siguiente codigo:" + userUID;
                requestSmsPermission(mensaje);
            }
        }
        else{
            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void subscribeToLocationUpdate() {
        if (mLocationUpdateDisposable != null && !mLocationUpdateDisposable.isDisposed()) return;
        mLocationUpdateDisposable = mViewModel.getLocationUpdates()
                .subscribe(this::onLocationUpdated, this::onLocationUpdateError);
    }

    private void onLocationUpdated(Location location) {
        String latlng = location.getLatitude() + "/" + location.getLongitude();
        mTextLatLng.setText(latlng);

        LatLng pos = new LatLng(location.getLatitude(), location.getLongitude());
        if (mGoogleMap == null) return;
        if (mMyLocMarker == null) {
            MarkerOptions options = new MarkerOptions().title("Me").position(pos);
            mMyLocMarker = mGoogleMap.addMarker(options);
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, DEFAULT_ZOOM_LEVEL));
        } else {
            mMyLocMarker.setPosition(pos);
            if (mFollowMarker) {
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(pos));
            } else if (!isLatLngOnVisibleRegion(pos)) {
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(pos));
            }
        }
    }

    private boolean isLatLngOnVisibleRegion(LatLng pos) {
        // check apakah pos berada pada posisi yg terlihat dilayar
        LatLngBounds bounds = mGoogleMap.getProjection().getVisibleRegion().latLngBounds;
        return bounds.contains(pos);
    }

    private void onLocationUpdateError(Throwable t) {
        if (t instanceof SecurityException) {
            // Access to coarse or fine location are not allowed by the user
            Util.checkLocationPermission(ShareLocationActivity.this);
        }
        Log.d(TAG, "LocationUpdateErr: " + t.toString());
    }

    private void sharingStateChange(Boolean isBroadcasting) {
        if (isBroadcasting) {
            mBtnBroadcast.setBackground(getResources().getDrawable(R.drawable.bg_btn_stop));
            mBtnBroadcast.setText(R.string.stop);
        } else {
            mBtnBroadcast.setBackground(getResources().getDrawable(R.drawable.bg_btn_start));
            mBtnBroadcast.setText(R.string.start);
        }
    }

    @SuppressLint("CheckResult")
    private void initViewModel() {
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ShareLocationViewModel.class);
        mViewModel.getSharingStateLiveData().observe(this, this::sharingStateChange);
        mViewModel.getDeviceIdObservable().subscribe(mTextDevId::setText);
    }

    @Override
    public void onResume() {
        super.onResume();
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
