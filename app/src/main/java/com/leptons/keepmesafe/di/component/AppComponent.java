package com.leptons.keepmesafe.di.component;

import com.leptons.keepmesafe.di.module.AppModule;
import com.leptons.keepmesafe.sharelocation.ShareLocationActivity;
import com.leptons.keepmesafe.tracklocation.TrackLocationActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(ShareLocationActivity client);

    void inject(TrackLocationActivity client);

}
