package com.leptons.keepmesafe.di.module;

import android.app.Application;
import android.content.Context;

import com.leptons.keepmesafe.ViewModelFactory;
import com.leptons.keepmesafe.data.DeviceLocationDataStore;
import com.leptons.keepmesafe.sharelocation.ShareLocationViewModel;
import com.leptons.keepmesafe.tracklocation.TrackLocationViewModel;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private Application mApplication;

    public AppModule(Application app) {
        mApplication = app;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    ShareLocationViewModel provideShareLocationViewModel() {
        return new ShareLocationViewModel(mApplication, new DeviceLocationDataStore());
    }

    @Provides
    @Singleton
    TrackLocationViewModel provideTrackLocationViewModel() {
        return new TrackLocationViewModel(new DeviceLocationDataStore());
    }

    @Provides
    @Singleton
    ViewModelFactory provideViewModelFactory(
            ShareLocationViewModel shareLocationViewModel,
            TrackLocationViewModel trackLocationViewModel) {
        return new ViewModelFactory(shareLocationViewModel, trackLocationViewModel);
    }

}
